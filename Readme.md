Gitlab Release
====

A script to populate Gitlab release notes.

<https://gitlab.com/claudiomattera/gitlab-release/>

This project is derived from [inetprocess/gitlab-release](https://github.com/inetprocess/gitlab-release).

Installation
----

### From source

This application can be installed with pip.

~~~~shell
pip3 install -r ./requirements.txt
pip3 install .
~~~~


Usage
----

The script takes the release name and description as command-line flags.
All remaining command-line arguments are taken as attachments: they are uploaded and linked in the release notes.

~~~~shell
gitlab-release --name "Name of the release" \
    --description "Markdown description of the release" \
    path/to/first/attachment \
    path/to/second/attachment \
    path/to/third/attachment
~~~~

For more information, use the `--help` flag:

~~~~shell
> gitlab-release --help
usage: gitlab-release [-h] [-v] [-n NAME] [-d DESCRIPTION] [--dry-run DRY_RUN]                                                                                                                                       
                      [files [files ...]]                                                                                                                                                                            

This program is intended to be used in a GitLab CI job in a Runner with Docker.

# 1. Configure your `.gitlab-ci.yml`

To make an automatic release you need to add something like this to the file
`.gitlab-ci.yml` in your project.
```yaml
stages:
    - build
    - publish
build:
    stage: build
    script:
        - my_build_command
    artifacts:
        expire_in: "1 hour"
        paths:
            - compiled-$CI_BUILD_TAG.exe
            - doc-$CI_BUILD_TAG.pdf
publish:
    image: inetprocess/gitlab-release
    stage: publish
    only:
        - tags
    script:
        - gitlab-release compiled-$CI_BUILD_TAG.exe doc-$CI_BUILD_TAG.pdf
```

# 2. Generate a personnal access token

Generate a new [Personal Access Token]
(https://docs.gitlab.com/ee/api/README.html#personal-access-tokens)
from your user profile.

# 3. Configure your project

Set a [secret variable](https://docs.gitlab.com/ce/ci/variables/#secret-variables)
in your project named `GITLAB_ACCESS_TOKEN` with the token you have generated in
the previous step.

positional arguments:
  files                 Files to link in the release.

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         increase output
  -n NAME, --name NAME  Release name (default is tag name)
  -d DESCRIPTION, --description DESCRIPTION
                        Markdown release notes
  --dry-run DRY_RUN     Do not actually call Gitlab APIs
~~~~


License
----

Copyright Claudio Mattera 2020

You are free to copy, modify, and distribute this application with attribution under the terms of the [MIT license]. See the [`License.txt`](./License.txt) file for details.

This project is derived from [inetprocess/gitlab-release](https://github.com/inetprocess/gitlab-release), which is released under the same license.

[Rust]: https://rust-lang.org/
[MIT license]: https://opensource.org/licenses/MIT
