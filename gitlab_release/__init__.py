# Copyright Claudio Mattera 2020.
# Distributed under the MIT License.
# See accompanying file License.txt, or online at
# https://opensource.org/licenses/MIT

import argparse
import os
import sys
from distutils.version import LooseVersion
from urllib.parse import urljoin, quote_plus
import logging
from typing import Text, Any, Dict, Iterable, Tuple, Optional, cast

import requests


Json = Dict[Text, Any]


def main() -> None:
    try:
        arguments = parse_arguments()
        setup_logging(arguments.verbose)
        logger = logging.getLogger(__name__)

        release = GitlabRelease(dry_run=arguments.dry_run)
        info = release.create_release(arguments.name, arguments.description, arguments.files)
        logger.info("Created release {tag_name} with description \"{description}\"".format_map(info))
    except GitlabReleaseError as err:
        logger.error(err)
        sys.exit(1)


class GitlabReleaseError(RuntimeError):
    pass


class GitlabRelease:
    def __init__(self, dry_run: bool = False) -> None:
        self.logger = logging.getLogger(__name__)
        self.dry_run = dry_run
        self.env = {}  # type: Dict[Text, Text]
        self.fetch_all_env()
        self.api_project_url = self.get_api_project_url()

    def fetch_env_var(self, name: Text, error_message: Text) -> Text:
        if name not in self.env:
            try:
                self.env[name] = os.environ[name]
            except KeyError:
                raise GitlabReleaseError("Missing environment variable \"{}\": {}".format(name, error_message))
        return self.env[name]

    def is_gitlab_v9(self) -> bool:
        gl_version = self.fetch_env_var("CI_SERVER_VERSION", "")
        return LooseVersion(gl_version) >= LooseVersion("9.0.0")

    def get_tag(self) -> Text:
        tag_env = "CI_BUILD_TAG"
        if self.is_gitlab_v9():
            tag_env = "CI_COMMIT_TAG"
        return self.fetch_env_var(tag_env, "Releases can only be created on tag build.")

    def fetch_all_env(self) -> None:
        env = {
            "CI_PROJECT_URL": "",
            "CI_PROJECT_ID": "",
            "GITLAB_ACCESS_TOKEN": "You must specifiy the private token linked to your GitLab account.\n"
                                   "You probably need to add a GITLAB_ACCESS_TOKEN variable to your project,\n"
                                   "and make sure the release is created on a protected tag build.",
        }
        for var, msg in env.items():
            self.fetch_env_var(var, msg)

    def get_api_project_url(self) -> Text:
        api_version = "v3"
        if self.is_gitlab_v9():
            api_version = "v4"
        return urljoin(
            self.env["CI_PROJECT_URL"],
            "/api/{}/projects/{}".format(api_version, self.env["CI_PROJECT_ID"])
        )

    def post_file(self, filename: Text) -> Tuple[Text, Text]:
        self.logger.info("Posting new file \"%s\"", filename)
        url = "/".join((self.api_project_url, "uploads"))
        files = {
            "file": open(filename, "rb"),
        }
        result = self.request("post", url, files=files)
        return (result["alt"], result["url"])

    def set_release(self, name: Optional[Text], description: Text, update: bool = False) -> Json:
        self.logger.info("Setting release notes")
        if update:
            method = "put"
            url = "/".join((self.api_project_url, "releases", self.get_tag()))
        else:
            method = "post"
            url = "/".join((self.api_project_url, "releases"))
        headers = {"Content-Type": "application/json"}
        body = {
            "tag_name": self.get_tag(),
            "description": description,
        }
        if name is not None:
            body["name"] = name
        result = self.request(method, url, headers=headers, json=body)
        return result

    def fetch_release(self) -> Json:
        url = "/".join((self.api_project_url, "repository/tags", self.get_tag()))
        result = self.request("get", url)
        return cast(Json, result["release"])

    def request(self, method: Text, url: Text, **kwargs: Any) -> Json:
        if "headers" not in kwargs:
            kwargs["headers"] = {}
        kwargs["headers"]["PRIVATE-TOKEN"] = self.env["GITLAB_ACCESS_TOKEN"]
        self.logger.debug("Sending %s request to %s", method, url)
        if "json" in kwargs:
            self.logger.debug("JSON body: \"%s\"", kwargs["json"])
        if self.dry_run:
            self.logger.debug("Skipping request on a dry run")
            self.logger.debug("Returning a dummy result")
            return {
                "release": {
                    "description": "v0.1.0",
                },
                "tag_name": "v0.1.0",
                "description": "description",
                "alt": "filename",
                "url": "file URL"
            }
        else:
            res = requests.request(method, url, **kwargs)
            res.raise_for_status()
            self.logger.debug("Received response \"%s\"", res.content)
            return cast(Json, res.json())

    def post_asset(self, name: Text, file_url: Text) -> None:
        self.logger.info("Posting new asset link \"%s\"", name)
        url = "/".join((self.api_project_url, "releases", self.get_tag(), "assets/links"))
        self.request("post", url, data={"name": name, "url": file_url})

    def create_release(self, name: Optional[Text], description: Text, files: Iterable[Text]) -> Json:
        existing_release = self.fetch_release()
        update = False
        if existing_release is not None:
            description = "{}\n\n{}".format(existing_release["description"], description)
            update = True
        release_res = self.set_release(name, description, update)

        for filename in files:
            file_name, file_path = self.post_file(filename)
            file_url = "/".join((self.env["CI_PROJECT_URL"], file_path))
            self.logger.debug("Posting file \"%s\" with url \"%s\"", file_name, file_url)
            self.post_asset(file_name, file_url)

        return release_res


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="""
This program is intended to be used in a GitLab CI job in a Runner with Docker.

# 1. Configure your `.gitlab-ci.yml`

To make an automatic release you need to add something like this to the file
`.gitlab-ci.yml` in your project.
```yaml
stages:
    - build
    - publish
build:
    stage: build
    script:
        - my_build_command
    artifacts:
        expire_in: "1 hour"
        paths:
            - compiled-$CI_BUILD_TAG.exe
            - doc-$CI_BUILD_TAG.pdf
publish:
    image: inetprocess/gitlab-release
    stage: publish
    only:
        - tags
    script:
        - gitlab-release compiled-$CI_BUILD_TAG.exe doc-$CI_BUILD_TAG.pdf
```

# 2. Generate a personnal access token

Generate a new [Personal Access Token]
(https://docs.gitlab.com/ee/api/README.html#personal-access-tokens)
from your user profile.

# 3. Configure your project

Set a [secret variable](https://docs.gitlab.com/ce/ci/variables/#secret-variables)
in your project named `GITLAB_ACCESS_TOKEN` with the token you have generated in
the previous step.
"""
    )
    parser.add_argument(
        "-v", "--verbose",
        action="count",
        help="increase output",
    )
    parser.add_argument(
        "-n", "--name",
        help="Release name (default is tag name)",
    )
    parser.add_argument(
        "-d", "--description",
        default="",
        help="Markdown release notes",
    )
    parser.add_argument(
        "files",
        nargs="*",
        help="Files to link in the release.",
    )
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="Do not actually call Gitlab APIs",
    )

    return parser.parse_args()


def setup_logging(verbose: Optional[int]) -> None:
    if verbose is None or verbose <= 0:
        level = logging.WARN
    elif verbose == 1:
        level = logging.INFO
    else:
        level = logging.DEBUG

    logging.basicConfig(level=level)


if __name__ == "__main__":
    main()
