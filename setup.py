#!/usr/bin/env python

# Copyright Claudio Mattera 2020.
# Copyright Center for Energy Informatics 2018.
# Distributed under the MIT License.
# See accompanying file License.txt, or online at
# https://opensource.org/licenses/MIT

import sys

from setuptools import setup

needs_pytest = {"pytest", "test", "ptr"}.intersection(sys.argv)
pytest_runner = ["pytest-runner"] if needs_pytest else []

setup(
    name="gitlab_release",
    version="0.1.2",
    description="A script to populate Gitlab release notes",
    long_description=open("Readme.md").read(),
    long_description_content_type="text/markdown",
    author="Claudio Giovanni Mattera",
    author_email="claudio@mattera.it",
    url="https://gitlab.com/claudiomattera/gitlab_release/",
    license="MIT",
    packages=[
        "gitlab_release",
    ],
    include_package_data=True,
    entry_points={
        "gui_scripts": [
            "gitlab-release = gitlab_release:main",
        ],
    },
    install_requires=[
        "requests",
    ],
    setup_requires=pytest_runner,
    tests_require=["pytest"],
)
